#pragma once
#include <array>
#include <iostream>
#include <iomanip>
#include <cmath>

/**
 * array-based board for 2048
 *
 * index (1-d form):
 *  (0)  (1)  (2)  (3)
 *  (4)  (5)  (6)  (7)
 *  (8)  (9) (10) (11)
 * (12) (13) (14) (15)
 *
 */
class board {
public:
	typedef uint32_t cell;
	typedef std::array<cell, 4> row;
	typedef std::array<row, 4> grid;
	typedef uint64_t data;
	typedef int reward;

public:
	board() : tile(), attr(0), prev_opcode(-1u), max_tile(3) {}
	board(const grid& b, data v = 0) : tile(b), attr(v), prev_opcode(-1u), max_tile(3) {}
	board(const board& b) = default;
	board& operator =(const board& b) = default;

	operator grid&() { return tile; }
	operator const grid&() const { return tile; }
	row& operator [](unsigned i) { return tile[i]; }
	const row& operator [](unsigned i) const { return tile[i]; }
	cell& operator ()(unsigned i) { return tile[i / 4][i % 4]; }
	const cell& operator ()(unsigned i) const { return tile[i / 4][i % 4]; }

	data info() const { return attr; }
	data info(data dat) { data old = attr; attr = dat; return old; }
	
	unsigned get_prev_opcode() const { return prev_opcode; }
	uint32_t get_max_tile() const { return max_tile; }

public:
	bool operator ==(const board& b) const { return tile == b.tile; }
	bool operator < (const board& b) const { return tile <  b.tile; }
	bool operator !=(const board& b) const { return !(*this == b); }
	bool operator > (const board& b) const { return b < *this; }
	bool operator <=(const board& b) const { return !(b < *this); }
	bool operator >=(const board& b) const { return !(*this < b); }

public:

	/**
	 * place a tile (index value) to the specific position (1-d form index)
	 * return 0 if the action is valid, or -1 if not
	 */
	reward place(unsigned pos, cell tile) {
		if (pos >= 16) return -1;
		if (tile>max_tile || tile<1) return -1;
		if (prev_opcode == -2u) return -1;	//game finish or not
		operator()(pos) = tile;
		prev_opcode = 5;
		return 0;
	}

	/**
	 * apply an action to the board
	 * return the reward of the action, or -1 if the action is illegal
	 */
	reward slide(unsigned opcode) {
		prev_opcode = opcode & 0b11;
		switch (opcode & 0b11) {
		case 0: return slide_up();
		case 1: return slide_right();
		case 2: return slide_down();
		case 3: return slide_left();
		default: return -1;
		}
	}
	
	void try_slide_left(){
		for (int r = 0; r < 4; r++) {	//from top to down
			auto& row = tile[r];
			
			for (int c = 1; c < 4; c++) {	//from left to right
				uint32_t tile = row[c]; //get a new tile in right side of row
				
				if(row[c-1]){
					if(tile<3){
						if(tile && (tile+row[c-1])==3){
							row[c-1] = 3;
							row[c] = 0;
						}
					}else{
						if(tile==row[c-1]){
							row[c-1]++;
							row[c] = 0;
						}
					}
				}else{
					row[c-1] = tile;
					row[c] = 0;
				}
			}
		}
	}

	
	/**
	*slide to left
	*check from left to right
	*/
	reward slide_left() {
		board prev = *this;
		reward score = 0;
		
		for (int r = 0; r < 4; r++) {	//from top to down
			auto& row = tile[r];
			
			for (int c = 1; c < 4; c++) {	//from left to right
				uint32_t tile = row[c]; //get a new tile in right side of row
				
				if(row[c-1]){
					if(tile<3){
						if(tile && (tile+row[c-1])==3){
							row[c-1] = 3;
							row[c] = 0;
							score += 3;
						}
					}else{
						if(tile==row[c-1]){
							row[c-1]++;
							row[c] = 0;
							//score
							tile-=2;
							max_tile = (max_tile < row[c-1]) ? row[c-1] : max_tile;
							score += std::pow(3,tile);
							if(row[c-1]==15) prev_opcode = -2u;	//player win
						}
					}
				}else{
					row[c-1] = tile;
					row[c] = 0;
				}
			}
		}
		
		if (*this != prev){	//game finish or not
			return score;
		}else{
			prev_opcode = -3u; //env win
			return  -1;
		}	
	}
	
	reward slide_right() {
		reflect_horizontal();
		reward score = slide_left();
		reflect_horizontal();
		return score;
	}
	reward slide_up() {
		rotate_right();
		reward score = slide_right();
		rotate_left();
		return score;
	}
	reward slide_down() {
		rotate_right();
		reward score = slide_left();
		rotate_left();
		return score;
	}

	void transpose() {
		for (int r = 0; r < 4; r++) {
			for (int c = r + 1; c < 4; c++) {
				std::swap(tile[r][c], tile[c][r]);
			}
		}
	}

	void reflect_horizontal() {
		for (int r = 0; r < 4; r++) {
			std::swap(tile[r][0], tile[r][3]);
			std::swap(tile[r][1], tile[r][2]);
		}
	}

	void reflect_vertical() {
		for (int c = 0; c < 4; c++) {
			std::swap(tile[0][c], tile[3][c]);
			std::swap(tile[1][c], tile[2][c]);
		}
	}

	/**
	 * rotate the board clockwise by given times
	 */
	void rotate(int r = 1) {
		switch (((r % 4) + 4) % 4) {
		default:
		case 0: break;
		case 1: rotate_right(); break;
		case 2: reverse(); break;
		case 3: rotate_left(); break;
		}
	}

	void rotate_right() { transpose(); reflect_horizontal(); } // clockwise
	void rotate_left() { transpose(); reflect_vertical(); } // counterclockwise
	void reverse() { reflect_horizontal(); reflect_vertical(); }

public:
	friend std::ostream& operator <<(std::ostream& out, const board& b) {
		out << "+------------------------+" << std::endl;
		for (auto& row : b.tile) {
			out << "|" << std::dec;
			for (auto t : row) out << std::setw(6) << ( (t>3)?(3<<(t-3)):t );//((1 << t) & -2u);
			out << "|" << std::endl;
		}
		out << "+------------------------+" << std::endl;
		return out;
	}

private:
	grid tile;
	data attr;
	unsigned prev_opcode;	//slide:0~3 place:5 first:-1u player_win: -2u env_win:-3u
	cell max_tile;
};
