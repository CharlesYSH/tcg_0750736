#pragma once
#include <string>
#include <random>
#include <sstream>
#include <vector>
#include <map>
#include <type_traits>
#include <algorithm>
#include "board.h"
#include "action.h"
#include <fstream>
#include "weight.h"

#include <iostream>
class agent {
public:
	agent(const std::string& args = "") {
		std::stringstream ss("name=unknown role=unknown " + args);
		for (std::string pair; ss >> pair; ) {
			std::string key = pair.substr(0, pair.find('='));
			std::string value = pair.substr(pair.find('=') + 1);
			meta[key] = { value };
		}
		hint_tile = 0;
	}
	virtual ~agent() {}
	virtual void open_episode(const std::string& flag = "") {}
	virtual void close_episode(const std::string& flag = "") {}
	virtual action take_action(const board& b) { return action(); }
	virtual bool check_for_win(const board& b) { return false; }

public:
	virtual std::string property(const std::string& key) const { return meta.at(key); }
	virtual void notify(const std::string& msg) { meta[msg.substr(0, msg.find('='))] = { msg.substr(msg.find('=') + 1) }; }
	virtual std::string name() const { return property("name"); }
	virtual std::string role() const { return property("role"); }
	
public:
	virtual int get_hint(){
		return hint_tile>3?4:hint_tile;
	}

protected:
	typedef std::string key;
	struct value {
		std::string value;
		operator std::string() const { return value; }
		template<typename numeric, typename = typename std::enable_if<std::is_arithmetic<numeric>::value, numeric>::type>
		operator numeric() const { return numeric(std::stod(value)); }
	};
	std::map<key, value> meta;
	board::cell hint_tile;
};


/**
 * base agent for agents with a random engine
 */
class random_agent : public agent {
public:
	random_agent(const std::string& args = "") : agent(args) {
		if (meta.find("seed") != meta.end())
			engine.seed(int(meta["seed"]));
	}
	virtual ~random_agent() {}

protected:
	std::default_random_engine engine;
};

/**
 * agent for TD learning
 */
/*class td_player : public weight_agent, public learning_agent{
public:
	td_player(const std::string& args = "") : weight_agent("name=td role=player " + args), alpha(0.1f), opcode({ 0, 1, 2, 3 }) {
		if (meta.find("alpha") != meta.end())
			alpha = float(meta["alpha"]);
	}
	
	virtual action take_action(const board& before) {
		for (int op : opcode) {
			board::reward reward = board(before).slide(op);
			if (reward != -1) return action::slide(op);
		}
		return action();
	}
	
protected:
	std::array<int, 4> opcode;
};*/


/**
 * base agent for agents with weight tables
 */
class weight_agent : public agent {
public:
	weight_agent(const std::string& args = "") : agent(args) {
		if (meta.find("init") != meta.end()) // pass init=... to initialize the weight
			init_weights(meta["init"]);
		if (meta.find("load") != meta.end()) // pass load=... to load from a specific file
			load_weights(meta["load"]);
	}
	virtual ~weight_agent() {
		if (meta.find("save") != meta.end()){ // pass save=... to save to a specific file
			save_weights(meta["save"]);
			std::cout<<"save weights: "<<meta["save"].value<<std::endl;
		}
	}

protected:
	virtual void init_weights(const std::string& info) {
		if (info=="zero"){
			net.emplace_back(65536,0.0f); // create an empty weight table with size 65536
			net.emplace_back(65536,0.0f); // create an empty weight table with size 65536
			net.emplace_back(16777216,0.0f);
			net.emplace_back(16777216,0.0f);
		} else if(info=="one") {
			net.emplace_back(65536,1.0f); // create an empty weight table with size 65536
			net.emplace_back(65536,1.0f); // create an empty weight table with size 65536
		} else {
			net.emplace_back(65536,0.0f); // create an empty weight table with size 65536
			net.emplace_back(65536,0.0f); // create an empty weight table with size 65536
			net.emplace_back(16777216,0.0f);
			net.emplace_back(16777216,0.0f);
			//net.emplace_back(65536); // create an empty weight table with size 65536
			// now net.size() == 2; net[0].size() == 65536; net[1].size() == 65536
		}
		//std::cout<<"cout S: "<<net[0][123]<<std::endl;
	}
	virtual void load_weights(const std::string& path) {
		std::ifstream in(path, std::ios::in | std::ios::binary);
		if (!in.is_open()) std::exit(-1);
		uint32_t size;
		in.read(reinterpret_cast<char*>(&size), sizeof(size));
		net.resize(size);
		for (weight& w : net) in >> w;
		in.close();
		//std::cout<<"cout S: "<<std::endl;
	}
	virtual void save_weights(const std::string& path) {
		std::ofstream out(path, std::ios::out | std::ios::binary | std::ios::trunc);
		if (!out.is_open()) std::exit(-1);
		uint32_t size = net.size();
		out.write(reinterpret_cast<char*>(&size), sizeof(size));
		for (weight& w : net) out << w;
		out.close();
	}

protected:
	std::vector<weight> net;
};


/**
 * base agent for agents with weight tables
 */
class env_weight_agent : public agent {
public:
	env_weight_agent(const std::string& args = "") : agent(args) {
		if (meta.find("init") != meta.end()) // pass init=... to initialize the weight
			init_weights(meta["init"]);
		if (meta.find("load") != meta.end()) // pass load=... to load from a specific file
			load_weights(meta["load"]);
		if (meta.find("seed") != meta.end())
			engine.seed(int(meta["seed"]));
	}
	virtual ~env_weight_agent() {
		if (meta.find("save") != meta.end()){ // pass save=... to save to a specific file
			save_weights(meta["save"]);
			std::cout<<"save weights: "<<meta["save"].value<<std::endl;
		}
	}

protected:
	virtual void init_weights(const std::string& info) {
		if (info=="zero"){
			net.emplace_back(65536,0.0f); // create an empty weight table with size 65536
			net.emplace_back(65536,0.0f); // create an empty weight table with size 65536
			
			net.emplace_back(65536,0.0f); // create an empty weight table with size 65536
			net.emplace_back(65536,0.0f); // create an empty weight table with size 65536
			
			net.emplace_back(65536,0.0f); // create an empty weight table with size 65536
			net.emplace_back(65536,0.0f); // create an empty weight table with size 65536
			
			net.emplace_back(65536,0.0f); // create an empty weight table with size 65536
			net.emplace_back(65536,0.0f); // create an empty weight table with size 65536
		} else if(info=="one") {
			net.emplace_back(65536,1.0f); // create an empty weight table with size 65536
			net.emplace_back(65536,1.0f); // create an empty weight table with size 65536
			
			net.emplace_back(65536,1.0f); // create an empty weight table with size 65536
			net.emplace_back(65536,1.0f); // create an empty weight table with size 65536
			
			net.emplace_back(65536,1.0f); // create an empty weight table with size 65536
			net.emplace_back(65536,1.0f); // create an empty weight table with size 65536
			
			net.emplace_back(65536,1.0f); // create an empty weight table with size 65536
			net.emplace_back(65536,1.0f); // create an empty weight table with size 65536
		} else {
			net.emplace_back(65536,0.0f); // create an empty weight table with size 65536
			net.emplace_back(65536,0.0f); // create an empty weight table with size 65536
			
			net.emplace_back(65536,0.0f); // create an empty weight table with size 65536
			net.emplace_back(65536,0.0f); // create an empty weight table with size 65536
			
			net.emplace_back(65536,0.0f); // create an empty weight table with size 65536
			net.emplace_back(65536,0.0f); // create an empty weight table with size 65536
			
			net.emplace_back(65536,0.0f); // create an empty weight table with size 65536
			net.emplace_back(65536,0.0f); // create an empty weight table with size 65536
			//net.emplace_back(65536); // create an empty weight table with size 65536
			// now net.size() == 2; net[0].size() == 65536; net[1].size() == 65536
		}
		//std::cout<<"cout S: "<<net[0][123]<<std::endl;
	}
	virtual void load_weights(const std::string& path) {
		std::ifstream in(path, std::ios::in | std::ios::binary);
		if (!in.is_open()) std::exit(-1);
		uint32_t size;
		in.read(reinterpret_cast<char*>(&size), sizeof(size));
		net.resize(size);
		for (weight& w : net) in >> w;
		in.close();
		//std::cout<<"cout S: "<<std::endl;
	}
	virtual void save_weights(const std::string& path) {
		std::ofstream out(path, std::ios::out | std::ios::binary | std::ios::trunc);
		if (!out.is_open()) std::exit(-1);
		uint32_t size = net.size();
		out.write(reinterpret_cast<char*>(&size), sizeof(size));
		for (weight& w : net) out << w;
		out.close();
	}

protected:
	std::vector<weight> net;
	std::default_random_engine engine;
};


/**
 * agent for TD learning player
 */
class learning_agent : public weight_agent{
public:
	learning_agent(const std::string& args = "") : weight_agent("name=td role=player " + args), alpha(0.1f/100), opcode({ 0, 1, 2, 3 }) {
		if (meta.find("alpha") != meta.end()) alpha = float(meta["alpha"]);
		//std::cout<<"arg: "<<args<<std::endl;
		//std::cout<<float(meta["alpha"])<<std::endl;
		
	}
	
	virtual action take_action(const board& before) {
		int choose_op = -1;
		int choose_reward = -1;
		float test_value = -INFINITY;
		
		int feature_R1, feature_R2, feature_R3, feature_R4;
		int feature_C1, feature_C2, feature_C3, feature_C4;
		int feature_6V11, feature_6V12, feature_6V13, feature_6V21, feature_6V22, feature_6V23;
		int feature_6H11, feature_6H12, feature_6H21, feature_6H22, feature_6H31, feature_6H32;
		
		for (int op : opcode) {	//U R D L
			board test_after(before);
			board::reward reward = test_after.slide(op);
			if (reward != -1){
				
				int test_R1 = (test_after(0)<<12)  + (test_after(1)<<8)  + (test_after(2)<<4)  + test_after(3);
				int test_R2 = (test_after(4)<<12)  + (test_after(5)<<8)  + (test_after(6)<<4)  + test_after(7);
				int test_R3 = (test_after(8)<<12)  + (test_after(9)<<8)  + (test_after(10)<<4) + test_after(11);
				int test_R4 = (test_after(12)<<12) + (test_after(13)<<8) + (test_after(14)<<4) + test_after(15);
				int test_C1 = (test_after(0)<<12)  + (test_after(4)<<8)  + (test_after(8)<<4)  + test_after(12);
				int test_C2 = (test_after(1)<<12)  + (test_after(5)<<8)  + (test_after(9)<<4)  + test_after(13);
				int test_C3 = (test_after(2)<<12)  + (test_after(6)<<8)  + (test_after(10)<<4) + test_after(14);
				int test_C4 = (test_after(3)<<12)  + (test_after(7)<<8)  + (test_after(11)<<4) + test_after(15);

				int test_6V11 = (test_after(0) <<20)  + (test_after(1)<<16)
							  + (test_after(4) <<12)  + (test_after(5)<<8)
							  + (test_after(8) <<4)   + (test_after(9));
                //3                               
				int test_6V12 = (test_after(1) <<20)  + (test_after(2)<<16)
							  + (test_after(5) <<12)  + (test_after(6)<<8)
							  + (test_after(9) <<4)   + (test_after(10));
                                               
				int test_6V13 = (test_after(3) <<20)  + (test_after(2)<<16)
							  + (test_after(7) <<12)  + (test_after(6)<<8)
							  + (test_after(11)<<4)   + (test_after(10));
							  
				int test_6V21 = (test_after(12)<<20)  + (test_after(13)<<16)
							  + (test_after(8) <<12)  + (test_after(9)<<8)
							  + (test_after(4) <<4)   + (test_after(5));
				//3
				int test_6V22 = (test_after(5)    )  + (test_after(6)<<4)
							  + (test_after(9) <<8)  + (test_after(10)<<12)
							  + (test_after(13)<<16) + (test_after(14)<<20);

				int test_6V23 = (test_after(15)<<20) + (test_after(14)<<16)
							  + (test_after(11)<<12) + (test_after(10)<<8)
							  + (test_after(7)<<4)   + (test_after(6));

				int test_6H11 = (test_after(0)<<20)  + (test_after(1)<<12)  + (test_after(2)<<4)
							  + (test_after(4)<<16)  + (test_after(5)<<8)   + (test_after(6)   );
							                                                
				int test_6H12 = (test_after(1)<<4)   + (test_after(2)<<12)  + (test_after(3)<<20)
							  + (test_after(5)   )   + (test_after(6)<<8)   + (test_after(7)<<16);
				//3			                                                
				int test_6H21 = (test_after(4)<<16)  + (test_after(5)<<8)   + (test_after(6)    )
							  + (test_after(8)<<20)  + (test_after(9)<<12)  + (test_after(10)<<4);
				//3			                                                
				int test_6H22 = (test_after(5)<<4)   + (test_after(6)<<12)  + (test_after(7)<<20)
							  + (test_after(9)   )   + (test_after(10)<<8)  + (test_after(11)<<16);
							                                                
				int test_6H31 = (test_after(8)<<16)  + (test_after(9)<<8 )  + (test_after(10)   )
							  + (test_after(12)<<20) + (test_after(13)<<12) + (test_after(14)<<4);
							  
				int test_6H32 = (test_after(9)    )  + (test_after(10)<<8)  + (test_after(11)<<16)
							  + (test_after(13)<<4)  + (test_after(14)<<12) + (test_after(15)<<20);
							  
							  
				
				float action_value = net[0][test_R1] + net[1][test_R2] + net[1][test_R3] + net[0][test_R4]
									+net[0][test_C1] + net[1][test_C2] + net[1][test_C3] + net[0][test_C4]
									+net[2][test_6V11] + net[3][test_6V12] + net[2][test_6V13] + net[2][test_6V21] + net[3][test_6V22] + net[2][test_6V23]
									+net[2][test_6H11] + net[2][test_6H12] + net[3][test_6H21] + net[3][test_6H22] + net[2][test_6H31] + net[2][test_6H32]
									+reward;
				
				if(action_value>test_value){
					test_value = action_value;
					choose_op = op;
					choose_reward = reward;
					
					feature_R1 = test_R1;
					feature_R2 = test_R2;
					feature_R3 = test_R3;
					feature_R4 = test_R4;
					feature_C1 = test_C1;
					feature_C2 = test_C2;
					feature_C3 = test_C3;
					feature_C4 = test_C4;
					feature_6V11 = test_6V11;
					feature_6V12 = test_6V12;
					feature_6V13 = test_6V13;
					feature_6V21 = test_6V21;
					feature_6V22 = test_6V22;
					feature_6V23 = test_6V23;
					feature_6H11 = test_6H11;
					feature_6H12 = test_6H12;
					feature_6H21 = test_6H21;
					feature_6H22 = test_6H22;
					feature_6H31 = test_6H31;
					feature_6H32 = test_6H32;
				}				
			}
		}
		
		if(choose_op != -1){
			
			learning_states.emplace_back(
			std::vector<int>{	feature_R1, feature_R2, feature_R3, feature_R4, feature_C1, feature_C2, feature_C3, feature_C4,
								feature_6V11, feature_6V12, feature_6V13, feature_6V21, feature_6V22, feature_6V23,
								feature_6H11, feature_6H12, feature_6H21, feature_6H22, feature_6H31, feature_6H32,
								choose_reward		}
			);
				
			return action::slide(choose_op);
		}
		return action();
	}
	
	
	
	
	virtual void update_weights_table(){
		
		float state_t_afrer  = 0;
		float state_t_before = 0;
		
		while (!learning_states.empty()){
			//std::cout<<learning_states.back()[8]<<"<-";
			
			board::reward reward = learning_states.back()[20];
			state_t_before =  net[0][learning_states.back()[0]] + net[0][learning_states.back()[3]] + net[0][learning_states.back()[4]] + net[0][learning_states.back()[7]]
				+ net[1][learning_states.back()[1]]  + net[1][learning_states.back()[2]]  + net[1][learning_states.back()[5]] + net[1][learning_states.back()[6]]
				+ net[2][learning_states.back()[8]]  + net[3][learning_states.back()[9]]  + net[2][learning_states.back()[10]]
				+ net[2][learning_states.back()[11]] + net[3][learning_states.back()[12]] + net[2][learning_states.back()[13]] 
				+ net[2][learning_states.back()[14]] + net[2][learning_states.back()[15]]  + net[3][learning_states.back()[16]]
				+ net[3][learning_states.back()[17]] + net[2][learning_states.back()[18]] + net[2][learning_states.back()[19]];
				
			float t_delta = reward + state_t_afrer - state_t_before;
			net[0][learning_states.back()[0]]  += alpha*t_delta;
			net[1][learning_states.back()[1]]  += alpha*t_delta;
			net[1][learning_states.back()[2]]  += alpha*t_delta;
			net[0][learning_states.back()[3]]  += alpha*t_delta;
			net[0][learning_states.back()[4]]  += alpha*t_delta;
			net[1][learning_states.back()[5]]  += alpha*t_delta;
			net[1][learning_states.back()[6]]  += alpha*t_delta;
			net[0][learning_states.back()[7]]  += alpha*t_delta;
			net[2][learning_states.back()[8]]  += alpha*t_delta;
			net[3][learning_states.back()[9]]  += alpha*t_delta;
			net[2][learning_states.back()[10]] += alpha*t_delta;
			net[2][learning_states.back()[11]] += alpha*t_delta;
			net[3][learning_states.back()[12]] += alpha*t_delta;
			net[2][learning_states.back()[13]] += alpha*t_delta;
			net[2][learning_states.back()[14]] += alpha*t_delta;
			net[2][learning_states.back()[15]] += alpha*t_delta;
			net[3][learning_states.back()[16]] += alpha*t_delta;
			net[3][learning_states.back()[17]] += alpha*t_delta;
			net[2][learning_states.back()[18]] += alpha*t_delta;
			net[2][learning_states.back()[19]] += alpha*t_delta;
			
			
			state_t_afrer = net[0][learning_states.back()[0]] + net[0][learning_states.back()[3]] + net[0][learning_states.back()[4]] + net[0][learning_states.back()[7]]
				+ net[1][learning_states.back()[1]] + net[1][learning_states.back()[2]] + net[1][learning_states.back()[5]] + net[1][learning_states.back()[6]]
				+ net[2][learning_states.back()[8]]  + net[3][learning_states.back()[9]]  + net[2][learning_states.back()[10]]
				+ net[2][learning_states.back()[11]] + net[3][learning_states.back()[12]] + net[2][learning_states.back()[13]] 
				+ net[2][learning_states.back()[14]] + net[2][learning_states.back()[15]]  + net[3][learning_states.back()[16]]
				+ net[3][learning_states.back()[17]] + net[2][learning_states.back()[18]] + net[2][learning_states.back()[19]];
			
			learning_states.pop_back();
		}
		//std::cout<<std::endl;
	}
	
	virtual void close_episode(const std::string& flag = "") {
		//std::cout<<"close play EP: "<<learning_states.size()<<std::endl;
		if(alpha) update_weights_table();
		
	}
protected:
	float alpha;
	std::array<int, 4> opcode;
	std::vector<std::vector<int>> learning_states;
};



/**
 * agent for TD learning environment
 */
class learning_env : public env_weight_agent{
public:
	learning_env(const std::string& args = "") : env_weight_agent("name=td_env role=environment " + args),
		space({ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 }),
		subspace_U({12, 13, 14, 15}), subspace_R({0, 4, 8, 12}), subspace_D({0, 1, 2, 3}), subspace_L({3, 7, 11, 15}),
		tile_bag({ 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3 }), bag_top(0), bonus(0,21), alpha(0.0f) {
			if (meta.find("alpha") != meta.end()) alpha = float(meta["alpha"]);
		}
	
	virtual action take_action(const board& after) {
		unsigned prev_op = after.get_prev_opcode();
		
		//generate tile
		if(!bag_top || prev_op==-1u){
			std::shuffle(tile_bag.begin(), tile_bag.end(), engine);
			bag_top = 12;
		}
		
		if(prev_op==-1u){
			hint_tile = tile_bag[--bag_top];
		}
		board::cell tile = hint_tile;
		
		board::cell max_tile = after.get_max_tile();
		if(max_tile>6){
			int ii = bonus(engine);
			
			if( ii ){ //if ii=[0]
				hint_tile = tile_bag[--bag_top];
			}else{	//if ii=[1,20]
				std::uniform_int_distribution<int> dis(4,max_tile-3);
				hint_tile = dis(engine);
			}
		}else{
			hint_tile = tile_bag[--bag_top];
		}
		//global_hint = hint_tile;
		
		//choose position
		int choose_pos = -1;
		float min_reward = -1;
		float min_value = INFINITY;
		int hint_net = get_hint()-1;
		float temp_reward = -1;
		
		switch (prev_op) {
			case 0: //up {12, 13, 14, 15}
			
				//choose best position
				std::shuffle(subspace_U.begin(), subspace_U.end(), engine);	//position
				for (int test_pos : subspace_U) {
					if (after(test_pos) != 0) continue;
					temp_reward = search_reward(after, test_pos, tile);
					if(temp_reward==-1){
						choose_pos = test_pos;
						//std::cout<<"best choose"<<std::endl;
						break;
					}else{
						float temp = search_value(after, hint_net, test_pos, tile)+temp_reward;
						if(temp<min_value){
							choose_pos = test_pos;
							min_value = temp;
							min_reward = temp_reward;
						}
					}
				}
				
				//save feature
				if(choose_pos!=-1){
					
					board next_before(after);
					next_before.place(choose_pos, tile);
					int test_R1 = (next_before(0)<<12)  + (next_before(1)<<8)  + (next_before(2)<<4)  + next_before(3);
					int test_R2 = (next_before(4)<<12)  + (next_before(5)<<8)  + (next_before(6)<<4)  + next_before(7);
					int test_R3 = (next_before(8)<<12)  + (next_before(9)<<8)  + (next_before(10)<<4) + next_before(11);
					int test_R4 = (next_before(12)<<12) + (next_before(13)<<8) + (next_before(14)<<4) + next_before(15);
					int test_C1 = (next_before(0)<<12)  + (next_before(4)<<8)  + (next_before(8)<<4)  + next_before(12);
					int test_C2 = (next_before(1)<<12)  + (next_before(5)<<8)  + (next_before(9)<<4)  + next_before(13);
					int test_C3 = (next_before(2)<<12)  + (next_before(6)<<8)  + (next_before(10)<<4) + next_before(14);
					int test_C4 = (next_before(3)<<12)  + (next_before(7)<<8)  + (next_before(11)<<4) + next_before(15);
					
					learning_states.emplace_back(std::vector<int>{test_R1,test_R2,test_R3,test_R4,test_C1,test_C2,test_C3,test_C4,hint_net});
					learning_reward.emplace_back(min_reward);
					
					return action::place(choose_pos, tile);
				}
				
			break;	//up
			case 1: //right {0, 4, 8, 12}
				//choose best position
				std::shuffle(subspace_R.begin(), subspace_R.end(), engine);	//position
				for (int test_pos : subspace_R) {
					if (after(test_pos) != 0) continue;
					temp_reward = search_reward(after, test_pos, tile);
					if(temp_reward==-1){
						choose_pos = test_pos;
						//std::cout<<"best choose"<<std::endl;
						break;
					}else{
						float temp = search_value(after, hint_net, test_pos, tile)+temp_reward;
						if(temp<min_value){
							choose_pos = test_pos;
							min_value = temp;
							min_reward = temp_reward;
						}
					}
				}
				
				//save feature
				if(choose_pos!=-1){
					
					board next_before(after);
					next_before.place(choose_pos, tile);
					int test_R1 = (next_before(0)<<12)  + (next_before(1)<<8)  + (next_before(2)<<4)  + next_before(3);
					int test_R2 = (next_before(4)<<12)  + (next_before(5)<<8)  + (next_before(6)<<4)  + next_before(7);
					int test_R3 = (next_before(8)<<12)  + (next_before(9)<<8)  + (next_before(10)<<4) + next_before(11);
					int test_R4 = (next_before(12)<<12) + (next_before(13)<<8) + (next_before(14)<<4) + next_before(15);
					int test_C1 = (next_before(0)<<12)  + (next_before(4)<<8)  + (next_before(8)<<4)  + next_before(12);
					int test_C2 = (next_before(1)<<12)  + (next_before(5)<<8)  + (next_before(9)<<4)  + next_before(13);
					int test_C3 = (next_before(2)<<12)  + (next_before(6)<<8)  + (next_before(10)<<4) + next_before(14);
					int test_C4 = (next_before(3)<<12)  + (next_before(7)<<8)  + (next_before(11)<<4) + next_before(15);
					
					learning_states.emplace_back(std::vector<int>{test_R1,test_R2,test_R3,test_R4,test_C1,test_C2,test_C3,test_C4,hint_net});
					learning_reward.emplace_back(min_reward);
					
					return action::place(choose_pos, tile);
				}
			break;	//right
			case 2: //down {0, 1, 2, 3}
				//choose best position
				std::shuffle(subspace_D.begin(), subspace_D.end(), engine);	//position
				for (int test_pos : subspace_D) {
					if (after(test_pos) != 0) continue;
					temp_reward = search_reward(after, test_pos, tile);
					if(temp_reward==-1){
						choose_pos = test_pos;
						//std::cout<<"best choose"<<std::endl;
						break;
					}else{
						float temp = search_value(after, hint_net, test_pos, tile)+temp_reward;
						if(temp<min_value){
							choose_pos = test_pos;
							min_value = temp;
							min_reward = temp_reward;
						}
					}
				}
				
				//save feature
				if(choose_pos!=-1){
					
					board next_before(after);
					next_before.place(choose_pos, tile);
					int test_R1 = (next_before(0)<<12)  + (next_before(1)<<8)  + (next_before(2)<<4)  + next_before(3);
					int test_R2 = (next_before(4)<<12)  + (next_before(5)<<8)  + (next_before(6)<<4)  + next_before(7);
					int test_R3 = (next_before(8)<<12)  + (next_before(9)<<8)  + (next_before(10)<<4) + next_before(11);
					int test_R4 = (next_before(12)<<12) + (next_before(13)<<8) + (next_before(14)<<4) + next_before(15);
					int test_C1 = (next_before(0)<<12)  + (next_before(4)<<8)  + (next_before(8)<<4)  + next_before(12);
					int test_C2 = (next_before(1)<<12)  + (next_before(5)<<8)  + (next_before(9)<<4)  + next_before(13);
					int test_C3 = (next_before(2)<<12)  + (next_before(6)<<8)  + (next_before(10)<<4) + next_before(14);
					int test_C4 = (next_before(3)<<12)  + (next_before(7)<<8)  + (next_before(11)<<4) + next_before(15);
					
					learning_states.emplace_back(std::vector<int>{test_R1,test_R2,test_R3,test_R4,test_C1,test_C2,test_C3,test_C4,hint_net});
					learning_reward.emplace_back(min_reward);
					
					return action::place(choose_pos, tile);
				}
			break;	//down
			case 3: //left {3, 7, 11, 15}
				//choose best position
				std::shuffle(subspace_L.begin(), subspace_L.end(), engine);	//position
				for (int test_pos : subspace_L) {
					if (after(test_pos) != 0) continue;
					temp_reward = search_reward(after, test_pos, tile);
					if(temp_reward==-1){
						choose_pos = test_pos;
						//std::cout<<"best choose"<<std::endl;
						break;
					}else{
						float temp = search_value(after, hint_net, test_pos, tile)+temp_reward;
						if(temp<min_value){
							choose_pos = test_pos;
							min_value = temp;
							min_reward = temp_reward;
						}
					}
				}
				
				//save feature
				if(choose_pos!=-1){
					
					board next_before(after);
					next_before.place(choose_pos, tile);
					int test_R1 = (next_before(0)<<12)  + (next_before(1)<<8)  + (next_before(2)<<4)  + next_before(3);
					int test_R2 = (next_before(4)<<12)  + (next_before(5)<<8)  + (next_before(6)<<4)  + next_before(7);
					int test_R3 = (next_before(8)<<12)  + (next_before(9)<<8)  + (next_before(10)<<4) + next_before(11);
					int test_R4 = (next_before(12)<<12) + (next_before(13)<<8) + (next_before(14)<<4) + next_before(15);
					int test_C1 = (next_before(0)<<12)  + (next_before(4)<<8)  + (next_before(8)<<4)  + next_before(12);
					int test_C2 = (next_before(1)<<12)  + (next_before(5)<<8)  + (next_before(9)<<4)  + next_before(13);
					int test_C3 = (next_before(2)<<12)  + (next_before(6)<<8)  + (next_before(10)<<4) + next_before(14);
					int test_C4 = (next_before(3)<<12)  + (next_before(7)<<8)  + (next_before(11)<<4) + next_before(15);
					
					learning_states.emplace_back(std::vector<int>{test_R1,test_R2,test_R3,test_R4,test_C1,test_C2,test_C3,test_C4,hint_net});
					learning_reward.emplace_back(min_reward);
					
					return action::place(choose_pos, tile);
				}
			break;	//left
			
			default: //This part should run at first 9 step.
				std::shuffle(space.begin(), space.end(), engine);
				for (int test_pos : space) {
					if (after(test_pos) != 0) continue;
					choose_pos = test_pos;
				}
				if(choose_pos!=-1) return action::place(choose_pos, tile);
			break;
		}
		
		return action();
	}
	
	float search_reward(const board& before, unsigned pos, int tile){
		float max_reward = -1;
		
		for(int op=0;op<4;op++){
			board after(before);
			after.place(pos, tile);
			board::reward reward = after.slide(op);
			if (reward > max_reward){		
				max_reward = reward;
			}
		}
		
		return max_reward;
		/*
		int num_valid = 0;
		
		for(int op=0;op<4;op++){
			board after(before);
			after.place(pos, tile);
			board::reward reward = after.slide(op);
			if (reward != -1){		
				sum_reward += reward;
				num_valid++;
			}
		}
		
		
		if(num_valid){
			return sum_reward/(float)num_valid;	//average or can choose max
		}else{
			return -1.0;
		}*/
	}
	
	float search_value(const board& after, int hint, unsigned pos, int tile){
		board test_before(after);
		test_before.place(pos, tile);
		int test_R1 = (test_before(0)<<12)  + (test_before(1)<<8)  + (test_before(2)<<4)  + test_before(3);
		int test_R2 = (test_before(4)<<12)  + (test_before(5)<<8)  + (test_before(6)<<4)  + test_before(7);
		int test_R3 = (test_before(8)<<12)  + (test_before(9)<<8)  + (test_before(10)<<4) + test_before(11);
		int test_R4 = (test_before(12)<<12) + (test_before(13)<<8) + (test_before(14)<<4) + test_before(15);
		int test_C1 = (test_before(0)<<12)  + (test_before(4)<<8)  + (test_before(8)<<4)  + test_before(12);
		int test_C2 = (test_before(1)<<12)  + (test_before(5)<<8)  + (test_before(9)<<4)  + test_before(13);
		int test_C3 = (test_before(2)<<12)  + (test_before(6)<<8)  + (test_before(10)<<4) + test_before(14);
		int test_C4 = (test_before(3)<<12)  + (test_before(7)<<8)  + (test_before(11)<<4) + test_before(15);
		float action_value = net[hint*2+0][test_R1] + net[hint*2+1][test_R2] + net[hint*2+1][test_R3] + net[hint*2+0][test_R4]
							+net[hint*2+0][test_C1] + net[hint*2+1][test_C2] + net[hint*2+1][test_C3] + net[hint*2+0][test_C4];
		
		return action_value;
	}
	
	virtual void update_weights_table(){
		float state_t_before = 0;
		float state_t_afrer  = 0;
		
		while (!learning_states.empty()){
			int hint_net = learning_states.back()[8]; //The weight of net want to update.
			float avg_reward = learning_reward.back();
			//std::cout<<"ar: "<<avg_reward<<std::endl;
			
			state_t_afrer =  net[hint_net*2+0][learning_states.back()[0]] + net[hint_net*2+0][learning_states.back()[3]] + net[hint_net*2+0][learning_states.back()[4]] + net[hint_net*2+0][learning_states.back()[7]]
					+ net[hint_net*2+1][learning_states.back()[1]]  + net[hint_net*2+1][learning_states.back()[2]]  + net[hint_net*2+1][learning_states.back()[5]] + net[hint_net*2+1][learning_states.back()[6]];
			
			float t_delta = avg_reward + state_t_before - state_t_afrer;
			net[hint_net*2+0][learning_states.back()[0]]  += alpha*t_delta;
			net[hint_net*2+1][learning_states.back()[1]]  += alpha*t_delta;
			net[hint_net*2+1][learning_states.back()[2]]  += alpha*t_delta;
			net[hint_net*2+0][learning_states.back()[3]]  += alpha*t_delta;
			net[hint_net*2+0][learning_states.back()[4]]  += alpha*t_delta;
			net[hint_net*2+1][learning_states.back()[5]]  += alpha*t_delta;
			net[hint_net*2+1][learning_states.back()[6]]  += alpha*t_delta;
			net[hint_net*2+0][learning_states.back()[7]]  += alpha*t_delta;
			
			state_t_before =  net[hint_net*2+0][learning_states.back()[0]] + net[hint_net*2+0][learning_states.back()[3]] + net[hint_net*2+0][learning_states.back()[4]] + net[hint_net*2+0][learning_states.back()[7]]
					+ net[hint_net*2+1][learning_states.back()[1]]  + net[hint_net*2+1][learning_states.back()[2]]  + net[hint_net*2+1][learning_states.back()[5]] + net[hint_net*2+1][learning_states.back()[6]];
			
			learning_states.pop_back();
			learning_reward.pop_back();
			
			//std::cout<<"H: "<<hint_net<<std::endl;
			//std::cout<<"update"<<std::endl;
		}
	}
	
	virtual void close_episode(const std::string& flag = "") {
		if(alpha) update_weights_table();
	}
	
private:
	std::array<int, 16> space;
	std::array<int, 4> subspace_U;
	std::array<int, 4> subspace_R;
	std::array<int, 4> subspace_D;
	std::array<int, 4> subspace_L;
	
	std::array<int, 12> tile_bag;
	int bag_top;
	std::uniform_int_distribution<int> bonus;
	
	float alpha;
	std::vector<std::vector<int>> learning_states;
	std::vector<float> learning_reward;
};


/**
	backup environment
 */
class B_agent : public weight_agent{
public:
	B_agent(const std::string& args = "") : weight_agent("name=weight role=environment " + args), alpha(0),
	bonus_number(0), total_number(0), bonus(0,20){
		if (meta.find("alpha") != meta.end()) alpha = float(meta["alpha"]);
	}
	
	virtual action take_action(const board& after) {
		unsigned prev_op = after.get_prev_opcode();
		
		//generate tile bag
		if(tile_bag.empty()){
			tile_bag = std::vector<int>{ 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3 };
			std::shuffle(tile_bag.begin(), tile_bag.end(), engine);
		}
		
		//First step: generate hint tile
		if(prev_op==-1u){
			tile_bag = std::vector<int>{ 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3 };
			std::shuffle(tile_bag.begin(), tile_bag.end(), engine);
			hint_tile = tile_bag.back();
			tile_bag.pop_back();
		}
		
		switch (prev_op) {
			case 0: //up
				space_bag = std::vector<int>{12, 13, 14, 15};
				return choose_position(after);
			break;
			
			case 1: //right
				space_bag = std::vector<int>{0, 4, 8, 12};
				return choose_position(after);
			break;
			
			case 2: //down
				space_bag = std::vector<int>{0, 1, 2, 3};
				return choose_position(after);
			break;
			
			case 3: //left
				space_bag = std::vector<int>{3, 7, 11, 15};
				return choose_position(after);
			break;
			
			default: //This part should run in first 9 step.
				std::vector<int> space{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
				std::shuffle(space.begin(), space.end(), engine);
				for (int pos : space) {
					if (after(pos) != 0) continue;
					int choose_tile = hint_tile;
					hint_tile = tile_bag.back();
					tile_bag.pop_back();
					total_number++;		//count bonus
					return action::place(pos, choose_tile);
				}
			break;
		}
		
		return action();
	}
	
private:
	action choose_position(const board& after){
		int choose_tile = hint_tile;
		float min_value = INFINITY;
		int   choose_position = -1;
		
		for(int pos: space_bag){
			if (after(pos) != 0) continue;
			board test_before(after);
			test_before.place(pos, choose_tile);
			float action_value = search_max(test_before);
			if (action_value <= min_value){
				action_value = min_value;
				choose_position = pos;
			}
		}
		
		if(choose_position != -1){
			board::cell max_tile = after.get_max_tile();
			if(max_tile>6 && bonus_number*21<total_number && 0){
				std::uniform_int_distribution<int> uni(4,max_tile-3);
				hint_tile = uni(engine);
				bonus_number++;
			}else{
				hint_tile = tile_bag.back();
				tile_bag.pop_back();
			}
			
			total_number++;		//count bonus
			//std::cout<<choose_position<<",\t"<<choose_tile<<std::endl;
			return action::place(choose_position, choose_tile);
		}
		
		return action();
	}
	
	float search_max(const board& test_before){
		
		float max_value = -INFINITY;
		
		for(int op=0;op<4;op++){
			board test_after(test_before);
			board::reward action_reward = test_after.slide(op);
			float action_value = search_value(test_after);
			if(action_reward>-1 && action_value+action_reward>max_value){
				max_value = action_value+action_reward;
			}
		}
		//std::cout<<"max: "<<max_value<<std::endl;
		return max_value;
		
	}
	
	float search_value(const board& test_before){
		
		int test_R1 = (test_before(0)<<12)  + (test_before(1)<<8)  + (test_before(2)<<4)  + test_before(3);
		int test_R2 = (test_before(4)<<12)  + (test_before(5)<<8)  + (test_before(6)<<4)  + test_before(7);
		int test_R3 = (test_before(8)<<12)  + (test_before(9)<<8)  + (test_before(10)<<4) + test_before(11);
		int test_R4 = (test_before(12)<<12) + (test_before(13)<<8) + (test_before(14)<<4) + test_before(15);
		int test_C1 = (test_before(0)<<12)  + (test_before(4)<<8)  + (test_before(8)<<4)  + test_before(12);
		int test_C2 = (test_before(1)<<12)  + (test_before(5)<<8)  + (test_before(9)<<4)  + test_before(13);
		int test_C3 = (test_before(2)<<12)  + (test_before(6)<<8)  + (test_before(10)<<4) + test_before(14);
		int test_C4 = (test_before(3)<<12)  + (test_before(7)<<8)  + (test_before(11)<<4) + test_before(15);

		int test_6V11 = (test_before(0) <<20)  + (test_before(1)<<16)
					  + (test_before(4) <<12)  + (test_before(5)<<8)
					  + (test_before(8) <<4)   + (test_before(9));                               
		int test_6V12 = (test_before(1) <<20)  + (test_before(2)<<16)
					  + (test_before(5) <<12)  + (test_before(6)<<8)
					  + (test_before(9) <<4)   + (test_before(10));						   
		int test_6V13 = (test_before(3) <<20)  + (test_before(2)<<16)
					  + (test_before(7) <<12)  + (test_before(6)<<8)
					  + (test_before(11)<<4)   + (test_before(10));		  
		int test_6V21 = (test_before(12)<<20)  + (test_before(13)<<16)
					  + (test_before(8) <<12)  + (test_before(9)<<8)
					  + (test_before(4) <<4)   + (test_before(5));
		int test_6V22 = (test_before(5)    )  + (test_before(6)<<4)
					  + (test_before(9) <<8)  + (test_before(10)<<12)
					  + (test_before(13)<<16) + (test_before(14)<<20);
		int test_6V23 = (test_before(15)<<20) + (test_before(14)<<16)
					  + (test_before(11)<<12) + (test_before(10)<<8)
					  + (test_before(7)<<4)   + (test_before(6));
		int test_6H11 = (test_before(0)<<20)  + (test_before(1)<<12)  + (test_before(2)<<4)
					  + (test_before(4)<<16)  + (test_before(5)<<8)   + (test_before(6)   );															
		int test_6H12 = (test_before(1)<<4)   + (test_before(2)<<12)  + (test_before(3)<<20)
					  + (test_before(5)   )   + (test_before(6)<<8)   + (test_before(7)<<16);		                                                
		int test_6H21 = (test_before(4)<<16)  + (test_before(5)<<8)   + (test_before(6)    )
					  + (test_before(8)<<20)  + (test_before(9)<<12)  + (test_before(10)<<4);		                                                
		int test_6H22 = (test_before(5)<<4)   + (test_before(6)<<12)  + (test_before(7)<<20)
					  + (test_before(9)   )   + (test_before(10)<<8)  + (test_before(11)<<16);														
		int test_6H31 = (test_before(8)<<16)  + (test_before(9)<<8 )  + (test_before(10)   )
					  + (test_before(12)<<20) + (test_before(13)<<12) + (test_before(14)<<4);			  
		int test_6H32 = (test_before(9)    )  + (test_before(10)<<8)  + (test_before(11)<<16)
					  + (test_before(13)<<4)  + (test_before(14)<<12) + (test_before(15)<<20);
					  
		float action_value = net[0][test_R1] + net[1][test_R2] + net[1][test_R3] + net[0][test_R4]
							+net[0][test_C1] + net[1][test_C2] + net[1][test_C3] + net[0][test_C4]
							+net[2][test_6V11] + net[3][test_6V12] + net[2][test_6V13] + net[2][test_6V21] + net[3][test_6V22] + net[2][test_6V23]
							+net[2][test_6H11] + net[2][test_6H12] + net[3][test_6H21] + net[3][test_6H22] + net[2][test_6H31] + net[2][test_6H32];
		
		return action_value;
		
	}
	
private:
	std::vector<int> tile_bag;
	std::vector<int> space_bag;
	float alpha;
	unsigned bonus_number;
	unsigned total_number;
	std::vector<std::vector<int>> learning_states;
	std::default_random_engine engine;
	std::uniform_int_distribution<int> bonus;
};


/**
 * random environment
 * add a new random tile to an empty cell
 * 2-tile: 90%
 * 4-tile: 10%
 */
class rndenv : public random_agent {
public:
	rndenv(const std::string& args = "") : random_agent("name=random role=environment " + args),
		space({ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 }),
		subspace_U({12, 13, 14, 15}), subspace_R({0, 4, 8, 12}), subspace_D({0, 1, 2, 3}), subspace_L({3, 7, 11, 15}),
		tile_bag({ 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3 }), bag_top(0), bonus(0,21) {
		}

	virtual action take_action(const board& after) {
		unsigned prev_op = after.get_prev_opcode();
		
		if(!bag_top || prev_op==-1u){
			std::shuffle(tile_bag.begin(), tile_bag.end(), engine);
			bag_top = 12;
		}
		
		if(prev_op==-1u){
			hint_tile = tile_bag[--bag_top];
		}
		board::cell tile = hint_tile;
		
		board::cell max_tile = after.get_max_tile();
		if(max_tile>6){
			int ii = bonus(engine);
			
			if( ii ){ //if ii=[0]
				hint_tile = tile_bag[--bag_top];
			}else{	//if ii=[1,20]
				std::uniform_int_distribution<int> dis(4,max_tile-3);
				hint_tile = dis(engine);
			}
		}else{
			hint_tile = tile_bag[--bag_top];
		}
		//global_hint = hint_tile;
		
		switch (prev_op) {
			case 0: //up
				std::shuffle(subspace_U.begin(), subspace_U.end(), engine);	//position
				for (int pos : subspace_U) {
					if (after(pos) != 0) continue;
					return action::place(pos, tile);
				}
			break;	//up
			case 1: //right
				std::shuffle(subspace_R.begin(), subspace_R.end(), engine);	//position
				for (int pos : subspace_R) {
					if (after(pos) != 0) continue;
					return action::place(pos, tile);
				}
			break;	//right
			case 2: //down
				std::shuffle(subspace_D.begin(), subspace_D.end(), engine);	//position
				for (int pos : subspace_D) {
					if (after(pos) != 0) continue;
					return action::place(pos, tile);
				}
			break;	//down
			case 3: //left
				std::shuffle(subspace_L.begin(), subspace_L.end(), engine);	//position
				for (int pos : subspace_L) {
					if (after(pos) != 0) continue;
					return action::place(pos, tile);
				}
			break;	//left
			
			default: //This part should not run in normal.
				std::shuffle(space.begin(), space.end(), engine);
				for (int pos : space) {
					if (after(pos) != 0) continue;
					return action::place(pos, tile);
				}
			break;
		}
		
		return action();
	}
	

private:
	std::array<int, 16> space;
	std::array<int, 4> subspace_U;
	std::array<int, 4> subspace_R;
	std::array<int, 4> subspace_D;
	std::array<int, 4> subspace_L;
	std::array<int, 12> tile_bag;
	int bag_top;
	std::uniform_int_distribution<int> bonus;
};


/**
 * dummy player
 * select a legal action randomly
 */
class player : public random_agent {
public:
	player(const std::string& args = "") : random_agent("name=dummy role=player " + args),
		opcode({ 0, 1, 2, 3 }) {}

	virtual action take_action(const board& before) {
				
		//std::shuffle(opcode.begin(), opcode.end(), engine);
		
		opcode.clear();
		unsigned int maxtile = 0;
		int maxindex = 0;
		for (int p=0;p<16;p++){
			if(before(p)>=maxtile){
				maxtile = before(p);
				maxindex = p;
			}
		}
		//U R D L
		if(maxindex<8){
			opcode.push_back(0);opcode.push_back(2);
		}else{
			opcode.push_back(2);opcode.push_back(0);
		}
		if(maxindex%4<2){
			opcode.push_back(3);opcode.push_back(1);
		}else{
			opcode.push_back(1);opcode.push_back(3);
		}
		
		int op_prefer = 0;
		board::reward reward_prefer = -1e9;
		board::reward reward;
		for (int op : opcode) {
			reward = board(before).slide(op);
			if (reward>reward_prefer){
				op_prefer = op;
				reward_prefer = reward;
			}
		}
		if (reward_prefer != -1) return action::slide(op_prefer);
				
		return action();
	}

private:
	std::vector<int> opcode;
};

#include <ctime>

class td_env : public weight_agent{
public:
	td_env(const std::string& args = "") : weight_agent("name=td role=environment " + args), alpha(0.0f), bonus(0,20){
		if (meta.find("alpha") != meta.end()) alpha = float(meta["alpha"]);
		if (meta.find("seed") != meta.end()) engine.seed(int(meta["seed"]));
		if (meta.find("seed") == meta.end()) engine.seed( (time(0)*7+29)%85931 );
	}
	
	virtual action take_action(const board& after) {
		unsigned prev_op = after.get_prev_opcode();
		
		//First step: generate hint tile
		if(prev_op==-1u){
			tile_bag = std::vector<int>{ 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3 };
			std::shuffle(tile_bag.begin(), tile_bag.end(), engine);
			hint_tile = tile_bag.back();
			tile_bag.pop_back();
			bonus_number = 0;
			total_number = 1;
		}
		
		if(tile_bag.empty()){
			tile_bag = std::vector<int>{ 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3 };
			std::shuffle(tile_bag.begin(), tile_bag.end(), engine);
		}
		
		switch (prev_op) {
			case 0:
				space_bag = std::vector<int>{12, 13, 14, 15};
				return choose_action(after);
			break;
			
			case 1:
				space_bag = std::vector<int>{0, 4, 8, 12};
				return choose_action(after);
			break;
			
			case 2:
				space_bag = std::vector<int>{0, 1, 2, 3};
				return choose_action(after);
			break;
			
			case 3:
				space_bag = std::vector<int>{3, 7, 11, 15};
				return choose_action(after);
			break;
			
			default:
				std::vector<int> space{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
				std::shuffle(space.begin(), space.end(), engine);
				for (int pos : space) {
					if (after(pos) != 0) continue;
					
					int choose_tile = hint_tile;
					hint_tile = tile_bag.back();
					tile_bag.pop_back();
					return action::place(pos, choose_tile);
				}
		}
		return action();
	}
	
	virtual void close_episode(const std::string& flag = "") {
		//std::cout<<"close play EP: "<<learning_states.size()<<std::endl;
		if(alpha) update_weights_table();
	}
	
private:	
	action choose_action(const board& after){
		
		int choose_tile = hint_tile;
		int choose_position = -1;
		int choose_hint = -1;
		int choose_reward;
		
		
		//std::set<int> hint_set( tile_bag.begin(), tile_bag.end() );
		
		float min_value = INFINITY;
		
		//int debug_1=-1;
		
		//choose position
		for(int pos : space_bag){
			if (after(pos) != 0) continue;
			
			//for(int hint : hint_set){
				
				board test_before(after);
				test_before.place(pos, choose_tile);		
				float temp_value = search_value(test_before);
				int temp_reward = -1;
				int temp_hint = -2;
				//std::cout<<"temp value : "<<temp_value<<std::endl;
				//std::cout<<"temp board : "<<std::endl;
				//int debug_2=-1;
				
				board::reward action_reward;
				float max_value = -INFINITY;
				
				for(int op=0;op<4;op++){
					board test_after(test_before);
					action_reward = test_after.slide(op);
					if(action_reward>-1 && action_reward+temp_value>max_value){
						//find min value hint.
						temp_hint = find_hint(test_after, op);
						
						//find min value hint.
						temp_reward = action_reward;
						max_value = action_reward+temp_value;
					}
				}
				
				float action_value = max_value;
				
				if(action_value<min_value){
					min_value = action_value;
					choose_position = pos;
					choose_reward = temp_reward;
					choose_hint = temp_hint;
					
					//debug_1 = debug_2;
					//choose_hint = hint;
				}
			//}
			//std::cout<<"max value : "<<max_value<<std::endl;
		}
		
		
		if(choose_position != -1){
			
			
			board::cell max_tile = after.get_max_tile();
			if( (max_tile>6) && (bonus_number*22<total_number) && (bonus(engine)==7) ){
				std::uniform_int_distribution<int> uni(4,max_tile-3);
				hint_tile = uni(engine);
				bonus_number++;
			}else{
				if(choose_hint<1){
					hint_tile = tile_bag.back();
					tile_bag.pop_back();
				}else{
					hint_tile = choose_hint;
					tile_bag.erase(std::find(tile_bag.begin(),tile_bag.end(),choose_hint));
				}
			}
			total_number++;
			
			//std::cout<<"min value : "<<min_value<<std::endl;
			//std::cout<<"get reward: "<<choose_reward<<std::endl;
			//std::cout<<"get hint: "<<choose_hint<<std::endl;
			//std::cout<<after<<std::endl;
			save_state(after, choose_position, choose_tile, choose_reward);		
			//tile_bag.erase(std::find(tile_bag.begin(),tile_bag.end(),hint_tile));
			return action::place(choose_position, choose_tile);
		}
		
		return action();
	}
	
	int find_hint(const board& test_after, int op){
		int choose_hint= 0;
		float min_value = INFINITY;
		
		for(int test_tile : tile_bag){
			
			switch (op) {
			case 0:
				for(int p : std::vector<int>{12, 13, 14, 15}){
					if (test_after(p) != 0) continue;
					board test_before(test_after);
					test_before.place(p, test_tile);
					float temp_value = search_value(test_before);
					if( temp_value<min_value ){
						choose_hint = test_tile;
						min_value = temp_value;
					}
				}
				
			break;
			
			case 1:
				for(int p : std::vector<int>{0, 4, 8, 12}){
					if (test_after(p) != 0) continue;
					board test_before(test_after);
					test_before.place(p, test_tile);
					float temp_value = search_value(test_before);
					if( temp_value<min_value ){
						choose_hint = test_tile;
						min_value = temp_value;
					}
				}
				
			break;
			
			case 2:
				for(int p : std::vector<int>{0, 1, 2, 3}){
					if (test_after(p) != 0) continue;
					board test_before(test_after);
					test_before.place(p, test_tile);
					float temp_value = search_value(test_before);
					if( temp_value<min_value ){
						choose_hint = test_tile;
						min_value = temp_value;
					}
				}
				
			break;
			
			case 3:
				for(int p : std::vector<int>{3, 7, 11, 15}){
					if (test_after(p) != 0) continue;
					board test_before(test_after);
					test_before.place(p, test_tile);
					float temp_value = search_value(test_before);
					if( temp_value<min_value ){
						choose_hint = test_tile;
						min_value = temp_value;
					}
				}
				
			break;
			}
		}
		
		
		return choose_hint;
	}
	
	float search_value(const board& test_board){
		int test_R1 = (test_board(0)<<12)  + (test_board(1)<<8)  + (test_board(2)<<4)  + test_board(3);
		int test_R2 = (test_board(4)<<12)  + (test_board(5)<<8)  + (test_board(6)<<4)  + test_board(7);
		int test_R3 = (test_board(8)<<12)  + (test_board(9)<<8)  + (test_board(10)<<4) + test_board(11);
		int test_R4 = (test_board(12)<<12) + (test_board(13)<<8) + (test_board(14)<<4) + test_board(15);
		int test_C1 = (test_board(0)<<12)  + (test_board(4)<<8)  + (test_board(8)<<4)  + test_board(12);
		int test_C2 = (test_board(1)<<12)  + (test_board(5)<<8)  + (test_board(9)<<4)  + test_board(13);
		int test_C3 = (test_board(2)<<12)  + (test_board(6)<<8)  + (test_board(10)<<4) + test_board(14);
		int test_C4 = (test_board(3)<<12)  + (test_board(7)<<8)  + (test_board(11)<<4) + test_board(15);

		int test_6V11 = (test_board(0) <<20)  + (test_board(1)<<16)
					  + (test_board(4) <<12)  + (test_board(5)<<8)
					  + (test_board(8) <<4)   + (test_board(9));                             
		int test_6V12 = (test_board(1) <<20)  + (test_board(2)<<16)
					  + (test_board(5) <<12)  + (test_board(6)<<8)
					  + (test_board(9) <<4)   + (test_board(10));						   
		int test_6V13 = (test_board(3) <<20)  + (test_board(2)<<16)
					  + (test_board(7) <<12)  + (test_board(6)<<8)
					  + (test_board(11)<<4)   + (test_board(10));				  
		int test_6V21 = (test_board(12)<<20)  + (test_board(13)<<16)
					  + (test_board(8) <<12)  + (test_board(9)<<8)
					  + (test_board(4) <<4)   + (test_board(5));
		int test_6V22 = (test_board(5)    )  + (test_board(6)<<4)
					  + (test_board(9) <<8)  + (test_board(10)<<12)
					  + (test_board(13)<<16) + (test_board(14)<<20);
		int test_6V23 = (test_board(15)<<20) + (test_board(14)<<16)
					  + (test_board(11)<<12) + (test_board(10)<<8)
					  + (test_board(7)<<4)   + (test_board(6));

		int test_6H11 = (test_board(0)<<20)  + (test_board(1)<<12)  + (test_board(2)<<4)
					  + (test_board(4)<<16)  + (test_board(5)<<8)   + (test_board(6)   );															
		int test_6H12 = (test_board(1)<<4)   + (test_board(2)<<12)  + (test_board(3)<<20)
					  + (test_board(5)   )   + (test_board(6)<<8)   + (test_board(7)<<16);			                                                
		int test_6H21 = (test_board(4)<<16)  + (test_board(5)<<8)   + (test_board(6)    )
					  + (test_board(8)<<20)  + (test_board(9)<<12)  + (test_board(10)<<4);		                                                
		int test_6H22 = (test_board(5)<<4)   + (test_board(6)<<12)  + (test_board(7)<<20)
					  + (test_board(9)   )   + (test_board(10)<<8)  + (test_board(11)<<16);												
		int test_6H31 = (test_board(8)<<16)  + (test_board(9)<<8 )  + (test_board(10)   )
					  + (test_board(12)<<20) + (test_board(13)<<12) + (test_board(14)<<4); 
		int test_6H32 = (test_board(9)    )  + (test_board(10)<<8)  + (test_board(11)<<16)
					  + (test_board(13)<<4)  + (test_board(14)<<12) + (test_board(15)<<20);
					  
		float action_value = net[0][test_R1] + net[1][test_R2] + net[1][test_R3] + net[0][test_R4]
							+net[0][test_C1] + net[1][test_C2] + net[1][test_C3] + net[0][test_C4]
							+net[2][test_6V11] + net[3][test_6V12] + net[2][test_6V13] + net[2][test_6V21] + net[3][test_6V22] + net[2][test_6V23]
							+net[2][test_6H11] + net[2][test_6H12] + net[3][test_6H21] + net[3][test_6H22] + net[2][test_6H31] + net[2][test_6H32];
		
							
		return 	action_value;			
	}
	
	void save_state(const board& after,int choose_position,int choose_tile,int choose_reward){
		board before(after);
		before.place(choose_position, choose_tile);
		learning_states.emplace_back(before);
		learning_rewards.emplace_back(choose_reward);
	}
	
	void update_weights_table(){
		
		float state_t_next = 1;
		float state_t_current  = 0;
		
		while (!learning_states.empty()){
			//std::cout<<"get reward: "<<learning_rewards.back()<<std::endl;
			board test_board = learning_states.back();
			int test_R1 = (test_board(0)<<12)  + (test_board(1)<<8)  + (test_board(2)<<4)  + test_board(3);
			int test_R2 = (test_board(4)<<12)  + (test_board(5)<<8)  + (test_board(6)<<4)  + test_board(7);
			int test_R3 = (test_board(8)<<12)  + (test_board(9)<<8)  + (test_board(10)<<4) + test_board(11);
			int test_R4 = (test_board(12)<<12) + (test_board(13)<<8) + (test_board(14)<<4) + test_board(15);
			int test_C1 = (test_board(0)<<12)  + (test_board(4)<<8)  + (test_board(8)<<4)  + test_board(12);
			int test_C2 = (test_board(1)<<12)  + (test_board(5)<<8)  + (test_board(9)<<4)  + test_board(13);
			int test_C3 = (test_board(2)<<12)  + (test_board(6)<<8)  + (test_board(10)<<4) + test_board(14);
			int test_C4 = (test_board(3)<<12)  + (test_board(7)<<8)  + (test_board(11)<<4) + test_board(15);

			int test_6V11 = (test_board(0) <<20)  + (test_board(1)<<16)
						  + (test_board(4) <<12)  + (test_board(5)<<8)
						  + (test_board(8) <<4)   + (test_board(9));                             
			int test_6V12 = (test_board(1) <<20)  + (test_board(2)<<16)
						  + (test_board(5) <<12)  + (test_board(6)<<8)
						  + (test_board(9) <<4)   + (test_board(10));						   
			int test_6V13 = (test_board(3) <<20)  + (test_board(2)<<16)
						  + (test_board(7) <<12)  + (test_board(6)<<8)
						  + (test_board(11)<<4)   + (test_board(10));				  
			int test_6V21 = (test_board(12)<<20)  + (test_board(13)<<16)
						  + (test_board(8) <<12)  + (test_board(9)<<8)
						  + (test_board(4) <<4)   + (test_board(5));
			int test_6V22 = (test_board(5)    )  + (test_board(6)<<4)
						  + (test_board(9) <<8)  + (test_board(10)<<12)
						  + (test_board(13)<<16) + (test_board(14)<<20);
			int test_6V23 = (test_board(15)<<20) + (test_board(14)<<16)
						  + (test_board(11)<<12) + (test_board(10)<<8)
						  + (test_board(7)<<4)   + (test_board(6));

			int test_6H11 = (test_board(0)<<20)  + (test_board(1)<<12)  + (test_board(2)<<4)
						  + (test_board(4)<<16)  + (test_board(5)<<8)   + (test_board(6)   );															
			int test_6H12 = (test_board(1)<<4)   + (test_board(2)<<12)  + (test_board(3)<<20)
						  + (test_board(5)   )   + (test_board(6)<<8)   + (test_board(7)<<16);			                                                
			int test_6H21 = (test_board(4)<<16)  + (test_board(5)<<8)   + (test_board(6)    )
						  + (test_board(8)<<20)  + (test_board(9)<<12)  + (test_board(10)<<4);		                                                
			int test_6H22 = (test_board(5)<<4)   + (test_board(6)<<12)  + (test_board(7)<<20)
						  + (test_board(9)   )   + (test_board(10)<<8)  + (test_board(11)<<16);												
			int test_6H31 = (test_board(8)<<16)  + (test_board(9)<<8 )  + (test_board(10)   )
						  + (test_board(12)<<20) + (test_board(13)<<12) + (test_board(14)<<4); 
			int test_6H32 = (test_board(9)    )  + (test_board(10)<<8)  + (test_board(11)<<16)
						  + (test_board(13)<<4)  + (test_board(14)<<12) + (test_board(15)<<20);
			
			state_t_current = net[0][test_R1] + net[1][test_R2] + net[1][test_R3] + net[0][test_R4]
									+net[0][test_C1] + net[1][test_C2] + net[1][test_C3] + net[0][test_C4]
									+net[2][test_6V11] + net[3][test_6V12] + net[2][test_6V13] + net[2][test_6V21] + net[3][test_6V22] + net[2][test_6V23]
									+net[2][test_6H11] + net[2][test_6H12] + net[3][test_6H21] + net[3][test_6H22] + net[2][test_6H31] + net[2][test_6H32];
			
			float t_delta = state_t_next + learning_rewards.back() - state_t_current;
			
			net[0][test_R1]  += alpha*t_delta;
			net[1][test_R2]  += alpha*t_delta;
			net[1][test_R3]  += alpha*t_delta;
			net[0][test_R4]  += alpha*t_delta;
			net[0][test_C1]  += alpha*t_delta;
			net[1][test_C2]  += alpha*t_delta;
			net[1][test_C3]  += alpha*t_delta;
			net[0][test_C4]  += alpha*t_delta;
			net[2][test_6V11] += alpha*t_delta;
			net[3][test_6V12] += alpha*t_delta;
			net[2][test_6V13] += alpha*t_delta;
			net[2][test_6V21] += alpha*t_delta;
			net[3][test_6V22] += alpha*t_delta;
			net[2][test_6V23] += alpha*t_delta;
			net[2][test_6H11] += alpha*t_delta;
			net[2][test_6H12] += alpha*t_delta;
			net[3][test_6H21] += alpha*t_delta;
			net[3][test_6H22] += alpha*t_delta;
			net[2][test_6H31] += alpha*t_delta;
			net[2][test_6H32] += alpha*t_delta;
			
			state_t_next = net[0][test_R1] + net[1][test_R2] + net[1][test_R3] + net[0][test_R4]
									+net[0][test_C1] + net[1][test_C2] + net[1][test_C3] + net[0][test_C4]
									+net[2][test_6V11] + net[3][test_6V12] + net[2][test_6V13] + net[2][test_6V21] + net[3][test_6V22] + net[2][test_6V23]
									+net[2][test_6H11] + net[2][test_6H12] + net[3][test_6H21] + net[3][test_6H22] + net[2][test_6H31] + net[2][test_6H32];
			
			//learning_states.back().~board();
			
			learning_states.pop_back();
			learning_rewards.pop_back();
		}
		//learning_states.clear();
		//learning_rewards.clear();
	}
	
private:
	float alpha;
	std::vector<int> tile_bag;
	std::vector<int> space_bag;
	std::default_random_engine engine;
	std::vector<board> learning_states;
	std::vector<int> learning_rewards;
	unsigned bonus_number;
	unsigned total_number;
	std::uniform_int_distribution<int> bonus;
};

