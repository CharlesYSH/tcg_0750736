import subprocess
import time 
import numpy as np

epoch = 90
for ep in range(1,epoch+1):
    seed = np.random.randint(2147483647)
    print("EP: %d, Seed: %d"%(ep,seed))
    #bashCommand = './3s --total=100000 --play="load=weight alpha=0" --evil="load=env_test alpha=0.001 save=env_test seed='+str(seed)+'"'
    bashCommand = './3s --total=200000 --play="load=weight alpha=0" --evil="load=env_td alpha=0.0001 save=env_td seed=2'+str(seed)+'"'
    process = subprocess.Popen(bashCommand, stdout=subprocess.PIPE, shell=True)
    output, error = process.communicate()
    #output = subprocess.check_output(['./3s',bashCommand])
    print(output.decode("utf-8") )
    #print("wait...")
    time.sleep(2)
    
